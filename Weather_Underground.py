#! /usr/bin/python
#Written By Tom Paulus, @tompaulus, www.tompaulus.com

from lib.Char_Plate.Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
from WUndergroundAPI import WebAPI
import time
import smbus
import os

token = '5ac31b07c4e9f079'

def wait(button):
    while not lcd.buttonPressed(button):
        time.sleep(.1)

units_Temp = 'fahrenheit'
#units_Temp = 'celsius'
units_Speed = 'mph'
#units_Speed = 'kph

lcd = Adafruit_CharLCDPlate(busnum = 1)
API = WebAPI()
lcd.clear()
lcd.backlight(lcd.ON)

locations ={'1s':'CA','1c':'Ramona','2s':'CA','2c':'San_Diego', '3s':'NY','3c':'New_York'}

location = 1
locationChange = True
display = 1
stateChange = False
json = ''
while True:


    if locationChange:
        json = API.getLocation(locations.get(str(location)+'s'),locations.get(str(location)+'c'),token)
        locationChange = False
    print location
    print display
    print json
    if display == 1:
        lcd.clear()
        high = API.high(json,units_Temp)
        low = API.low(json,units_Temp)
        windSpeed = API.windSpeed(json,units_Speed)
        windDir = API.winDir(json)
        lcd.message('Temp H:' + high + ' L:' + low + '\nWind ' + windSpeed + ' ' + units_Speed + ' ' + windDir)

    if display == 2:
        lcd.clear()
        lcd.message('Precipitation\nHumidity')

    if display == 3:
        lcd.clear()
        lcd.message('TBD')
        
    while True:
        if lcd.buttonPressed(lcd.DOWN):
            display = display + 1
            if display == 4:
                display = 1
            break

        if lcd.buttonPressed(lcd.UP):
            display = display -1
            if display == 0:
                display = 3
            break

    wait(lcd.SELECT)
    lcd.clear()
    lcd.backlight(lcd.OFF)
    break

quit()